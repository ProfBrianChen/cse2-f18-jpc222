////////////////////////////
///HW05 by Jack Curtis
///CSE 002 with Professor Carr
///Due 10/9/2018
///Function:
///Genrates hands of
///cards in Poker
///
///Usage:
///Enter number of
///hands You Want
///to generate
//

import java.util.Scanner;

public class HW05{
  public static void main(String[]args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter number of hands you wish to generate as an Integer: ");
    int numHands = 0;
    while(true){
        boolean correctHands = myScanner.hasNextInt();
      if(correctHands){
        numHands = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Please enter an Integer value: ");
        myScanner.next();
        correctHands = myScanner.hasNextInt();
      }
    }
    
    
    int i = 1;
    int numPairs=0;
    int numTOK=0;
    
    int cardOneSuit = (int) (((Math.random() * 51) + 0) % 4);//generates a random number between 0 and 3
    int cardTwoSuit = (int) (((Math.random() * 51) + 0) % 4);
    int cardThreeSuit = (int) (((Math.random() * 51) + 0) % 4);
    int cardFourSuit = (int) (((Math.random() * 51) + 0) % 4);
    int cardFiveSuit = (int) (((Math.random() * 51) + 0) % 4);
   
    int cardOneNumber = (int) (((Math.random() * 51) + 0) % 13);//generates a random number between 0 and 12
    int cardTwoNumber = (int) (((Math.random() * 51) + 0) % 13);
    int cardThreeNumber = (int) (((Math.random() * 51) + 0) % 13);
    int cardFourNumber = (int) (((Math.random() * 51) + 0) % 13);
    int cardFiveNumber = (int) (((Math.random() * 51) + 0) % 13);
    
    String suitOne = "";//creates the suit variable
    String suitTwo = "";//creates the suit variable
    String suitThree = "";//creates the suit variable
    String suitFour = "";//creates the suit variable
    String suitFive = "";//creates the suit variable
    
    String typeOne = "";//creates the type variable
    String typeTwo = "";//creates the type variable
    String typeThree = "";//creates the type variable
    String typeFour = "";//creates the type variable
    String typeFive = "";//creates the type variable
    
    while(i<=numHands){
    cardOneSuit = (int) (((Math.random() * 51) + 0) % 4);//generates a random number between 0 and 3
    cardTwoSuit = (int) (((Math.random() * 51) + 0) % 4);
    cardThreeSuit = (int) (((Math.random() * 51) + 0) % 4);
    cardFourSuit = (int) (((Math.random() * 51) + 0) % 4);
    cardFiveSuit = (int) (((Math.random() * 51) + 0) % 4);
   
    cardOneNumber = (int) (((Math.random() * 51) + 0) % 13);//generates a random number between 0 and 12
    cardTwoNumber = (int) (((Math.random() * 51) + 0) % 13);
    cardThreeNumber = (int) (((Math.random() * 51) + 0) % 13);
    cardFourNumber = (int) (((Math.random() * 51) + 0) % 13);
    cardFiveNumber = (int) (((Math.random() * 51) + 0) % 13);
    switch(cardOneNumber){//opens switch statement for diamonds
      case 0: typeOne = "Ace";
        break;
      case 1: typeOne = "2";
        break;
      case 2: typeOne = "3";
        break;
      case 3: typeOne = "4";
        break;
      case 4: typeOne = "5";
        break;
      case 5: typeOne = "6";
        break;
      case 6: typeOne = "7";
        break;
      case 7: typeOne = "8";
        break;
      case 8: typeOne = "9";
        break;
      case 9: typeOne = "10";
        break;
      case 10: typeOne = "Jack";
        break;
      case 11: typeOne = "Queen";
        break;
      case 12: typeOne = "King";
        break;
    }//closes the cardNumber switch statement
   
    switch(cardTwoNumber){//opens switch statement for diamonds
      case 0: typeTwo = "Ace";
        break;
      case 1: typeTwo = "2";
        break;
      case 2: typeTwo = "3";
        break;
      case 3: typeTwo = "4";
        break;
      case 4: typeTwo = "5";
        break;
      case 5: typeTwo = "6";
        break;
      case 6: typeTwo = "7";
        break;
      case 7: typeTwo = "8";
        break;
      case 8: typeTwo = "9";
        break;
      case 9: typeTwo = "10";
        break;
      case 10: typeTwo = "Jack";
        break;
      case 11: typeTwo = "Queen";
        break;
      case 12: typeTwo = "King";
        break;
    }//closes the cardNumber switch statement
   
    switch(cardThreeNumber){//opens switch statement for diamonds
      case 0: typeThree = "Ace";
        break;
      case 1: typeThree = "2";
        break;
      case 2: typeThree = "3";
        break;
      case 3: typeThree = "4";
        break;
      case 4: typeThree = "5";
        break;
      case 5: typeThree = "6";
        break;
      case 6: typeThree = "7";
        break;
      case 7: typeThree = "8";
        break;
      case 8: typeThree = "9";
        break;
      case 9: typeThree = "10";
        break;
      case 10: typeThree = "Jack";
        break;
      case 11: typeThree = "Queen";
        break;
      case 12: typeThree = "King";
        break;
    }//closes the cardNumber switch statement
   
    switch(cardFourNumber){//opens switch statement for diamonds
      case 0: typeFour = "Ace";
        break;
      case 1: typeFour = "2";
        break;
      case 2: typeFour = "3";
        break;
      case 3: typeFour = "4";
        break;
      case 4: typeFour = "5";
        break;
      case 5: typeFour = "6";
        break;
      case 6: typeFour = "7";
        break;
      case 7: typeFour = "8";
        break;
      case 8: typeFour = "9";
        break;
      case 9: typeFour = "10";
        break;
      case 10: typeFour = "Jack";
        break;
      case 11: typeFour = "Queen";
        break;
      case 12: typeFour = "King";
        break;
    }//closes the cardNumber switch statement
   
    switch(cardFiveNumber){//opens switch statement for diamonds
      case 0: typeFive = "Ace";
        break;
      case 1: typeFive = "2";
        break;
      case 2: typeFive = "3";
        break;
      case 3: typeFive = "4";
        break;
      case 4: typeFive = "5";
        break;
      case 5: typeFive = "6";
        break;
      case 6: typeFive = "7";
        break;
      case 7: typeFive = "8";
        break;
      case 8: typeFive = "9";
        break;
      case 9: typeFive = "10";
        break;
      case 10: typeFive = "Jack";
        break;
      case 11: typeFive = "Queen";
        break;
      case 12: typeFive = "King";
        break;
    }//closes the cardNumber switch statement
    
    
    
    switch(cardOneSuit){//opens the switch statement for cards
      case 0: suitOne = "Diamonds";
        break;
      case 1: suitOne = "Clubs";
        break;
      case 2: suitOne = "Hearts";
        break;
      case 3: suitOne = "Spades";
        break;
    }//closes the suits switch statement
    
    switch(cardTwoSuit){//opens the switch statement for cards
      case 0: suitTwo = "Diamonds";
        break;
      case 1: suitTwo = "Clubs";
        break;
      case 2: suitTwo = "Hearts";
        break;
      case 3: suitTwo = "Spades";
        break;
    }//closes the suits switch statement
    
    switch(cardThreeSuit){//opens the switch statement for cards
      case 0: suitThree = "Diamonds";
        break;
      case 1: suitThree = "Clubs";
        break;
      case 2: suitThree = "Hearts";
        break;
      case 3: suitThree = "Spades";
        break;
    }//closes the suits switch statement
    
    switch(cardFourSuit){//opens the switch statement for cards
      case 0: suitFour = "Diamonds";
        break;
      case 1: suitFour = "Clubs";
        break;
      case 2: suitFour = "Hearts";
        break;
      case 3: suitFour = "Spades";
        break;
    }//closes the suits switch statement
    
    switch(cardFiveSuit){//opens the switch statement for cards
      case 0: suitFive = "Diamonds";
        break;
      case 1: suitFive = "Clubs";
        break;
      case 2: suitFive = "Hearts";
        break;
      case 3: suitFive = "Spades";
        break;
    }//closes the suits switch statement
   
    
    String cardOne = (typeOne) + (suitOne);
    String cardTwo = (typeTwo) + (suitTwo);
    String cardThree = (typeThree) + (suitThree);
    String cardFour = (typeFour) + (suitFour);
    String cardFive = (typeFive) + (suitFive);
    
    
    if(cardOne==cardTwo||cardOne==cardThree||cardOne==cardFour||cardOne==cardFive||cardTwo==cardThree||cardTwo==cardFour||cardTwo==cardFive||cardThree==cardFour||cardThree==cardFive||cardFour==cardFive){
      numPairs++;
    }
    else if(cardOne == cardTwo && cardOne == cardThree||cardOne == cardTwo && cardOne == cardFour||cardOne == cardTwo && cardOne == cardFive||cardOne == cardThree && cardOne == cardFour||cardOne == cardThree && cardOne == cardFive|| cardOne == cardFour && cardOne == cardFive){
      numTOK++;
    }
    else if(cardTwo == cardOne && cardTwo == cardThree||cardTwo == cardOne && cardTwo == cardFour||cardTwo == cardOne && cardTwo == cardFive||cardTwo == cardThree && cardTwo == cardFour||cardTwo == cardThree && cardTwo == cardFive|| cardTwo == cardFour && cardTwo == cardFive){
      numTOK++;
    }
    else if(cardThree == cardTwo && cardThree == cardOne||cardThree == cardTwo && cardThree == cardFour||cardThree == cardTwo && cardThree == cardFive||cardThree == cardOne && cardThree == cardFour||cardThree == cardOne && cardThree == cardFive|| cardThree == cardFour && cardThree == cardFive){
      numTOK++;
    }
    else if(cardFour == cardTwo && cardFour == cardThree||cardFour == cardTwo && cardFour == cardOne||cardFour == cardTwo && cardFour == cardFive||cardFour == cardThree && cardFour == cardOne||cardFour == cardThree && cardFour == cardFive|| cardFour == cardOne && cardFour == cardFive){
      numTOK++;
    }
    else if(cardFive == cardTwo && cardFive == cardThree||cardFive == cardTwo && cardFive == cardFour||cardFive == cardTwo && cardFive == cardOne||cardFive == cardThree && cardFive == cardFour||cardFive == cardThree && cardFive == cardOne|| cardFive == cardFour && cardFive == cardOne){
      numTOK++;
    }
  
    System.out.println(cardOne);
    System.out.println(cardTwo);
    System.out.println(cardThree);
    System.out.println(cardFour);
    System.out.println(cardFive);
    
    i++;
  }
 System.out.println(numPairs +" "+ numTOK);
  } 
 }