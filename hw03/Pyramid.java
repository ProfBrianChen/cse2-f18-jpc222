/////////////////////
///Pyramid by Jack Curtis
///CSE002 with Professor Carr
///September 15 2018
///Function:
///This program is meant to take
///the side length of a pyramid base
///and the height of the same pyramid
///and then to calculate its volume.
///Usage:
///Upon running user will be prompted to input
///a side lenght. Then they will be prompted to
///input a height. The program will then calculate
///and print the volume of the pyramid to the 
///screen.

import java.util.Scanner;//Importing the scanner

public class Pyramid{                     //Opening the public class
  public static void main(String[]args){  //Opening the main method
    
    Scanner myScanner = new Scanner(System.in);//Creating the scanner object
    
    System.out.print("The square side of the pyramid is: ");//prompting the user to put in the side length
    
    double sideLength = myScanner.nextDouble();//Creating and defining the double side Length
    
    System.out.print("The height of the pyramid is: ");//prompting the user to input the height of the pyramid
    
    double height = myScanner.nextDouble();//Creating and defining the double height
    
    double volume = (sideLength * sideLength * height) / 3;//Creating and defining the double volume
    
    System.out.println("The volume inside the pyramid is: " + volume);//Printing the pyramid volume to the screen
 
  }//closing the main method
}//closing the public class