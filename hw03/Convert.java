///////////////////////////////
///Convert by Jack Curtis
///CSE 002 with Professor Carr
///September 15 2018
///Function:
///The purpose of this program is
///to calculate the volume of rainfall
///in a given area in the form of
///cubic miles.
///Usage:
///If run the program will prompt the user
///to input a value in acres and then a value
///in inches to represent average rainfall.
///The program will then print the 
///total volume of the rain within the 
///acres in the form of cubic miles.

import java.util.Scanner;//importing the scanner program

public class Convert{                     //Creating the class
  public static void main(String[]argss){ //Setting the main method for java
    
    Scanner myScanner = new Scanner(System.in);//declaring an instance of the scanner object
    
    System.out.print("Enter the effected area in acres: ");//Prompting the user to input the number of acres
    
    double numAcres = myScanner.nextDouble();//creates an defines the double numAcres
    
    System.out.print("Enter the inches of rainfall in the affected area: ");//Prompting the user to input the rainfall in affected area
    
    double rainfallIn = myScanner.nextDouble();//creates and defines the double rainfall
    
    double numSquareMi = numAcres * 0.0015625;//Converts numAcres to squaremiles
    
    double rainfallMi = rainfallIn * 1.57828e-5;//Converts rainfallIn to miles
    
    float rainfallCubicMi = (float) (numSquareMi * rainfallMi);//Computing the volume of rain in cubic miles
    
    System.out.println(rainfallCubicMi + " cubic miles of rainfall");//Prints the rainfall in cubic miles
   
  }//closing the main method
}//closing the public class