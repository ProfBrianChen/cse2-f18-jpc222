////////////////////////////
///Hw07 methods practice
///By Jack Curtis
///CSE 002
///Due 10/30/2018
///Function:
///Takes an inputted text
///and prints a menu which
///allows the user to choose 
///from many options
///to manipulate their
///input
//

import java.util.Scanner;//importing scanner

public class hw07{//opening the public class
  public static void main(String[]args){//opening main method
    
    Scanner myScanner = new Scanner(System.in);//constructing/defining Scanner
    
    System.out.println("Please enter your statements: ");//prompt user
    String n2 = myScanner.nextLine();//defining user input
    sampleText(n2);//calling sampleText method
    
    
    int numChar = 0;//assigning numChar a value
    int numWords = 0;//assigning numWords a value
      
      while(true){//opening an infinite loop
        printMenu(n2);//calling printMenu
        String choice = myScanner.next();//defining user input in menu
      
        if(choice.equals("q")){//user quitting menu
        System.out.println("You've selected to quit. Goodbye");
        break;//breaking out cause quit
      }
        
     else if(choice.equals("c")){//finding white space
        numChar = getNumOfNonWSCharacters(n2);//calling the get Non WS method
        System.out.println("Number of non-whitespace characters: " + numChar);//printing result
        }
     
     else if(choice.equals("w")){//finding word count
       numWords = getNumOfWords(n2);//calling numWords method
       System.out.println("Number of words: " + numWords);//print output
     }
     
     else if(choice.equals("f")){//find a specific substring method
       System.out.println("Enter a word to be found: ");//prompt input
       String word = myScanner.next();//take input
       int instances = findText(word, n2);//calling findText
       System.out.println("\""+word+"\"" + " instances: " + instances);//print output
       }
     
     else if(choice.equals("r")){//replaces all '!' with '.'
       String newN2 = replaceExclamation(n2);//calls method for that
       System.out.println("Edited text: " + newN2);//print output
     }
     
     else if(choice.equals("s")){//shortens spaces greater that one to one
       String newN2 = shortSpace(n2);//calling shortspace
       System.out.println("Edited text: " + newN2);//print output
     }
     
      else{//asking for a correct choice
        System.out.println("Please enter a valid choice.");
        }
      
      }//closing infinite loop
     }//closing main method
  
  
  public static String sampleText(String n1){//open sampleText method
    
       System.out.println("You've entered: " + n1);//prints out user input
       return n1;//returning user input with meaningful state
    }    //closing sampleText


    public static void printMenu(String n2){//opening printMenu method
          
      System.out.println("MENU");//prints title
      System.out.println("c - Number of non-whitespace characters");//prints num nonWS choice
      System.out.println("w - Number of words");//prints num words choice
      System.out.println("f - Find text");//prints find text choice
      System.out.println("r - Replace all !'s");//prints replace ! choice
      System.out.println("s - Shorten spaces");//prints short space choice
      System.out.println("q - Quit");//prints quit choice
      System.out.println();//new line
      System.out.println("Choose an option: ");//prompts user input
    }//closing printMenu
    
    
    public static int getNumOfNonWSCharacters(String n2){//opening getNumOfNonWSCharacters method
      int numChar = 0;//creates numChar
      char charWS = ' ';//creates char charWS
      int j = 0;//creates counter int j
      int charTot = n2.length();//defines charTot as number of chars in n2
      
      while(charTot>0){//loop goes til charTot is 0 to run through all chars
      charWS = n2.charAt(charTot-1);//reads each char as loop runs

      charTot--;//decrements charTot to prevent infinite
      
      if(Character.isWhitespace(charWS)){//reads if a char is WS
        j++;//if true increments counter j
        }
     }//closing while loop
      
      charTot = n2.length();//redefines charTot so total char count present
      numChar = charTot - j;//defines numChar as total chars minus whitespace chars
      
      return numChar;//returns numChar
    }//closing getNumOfNonWSCharacters
     
    
    public static int getNumOfWords(String n2){//opening getNumOfWords method
      int numWords = 0;//creates numWords
      int charTot = n2.length();//defines character total
      char charWS = ' ';//defines char charWS
      int j =0;//creates int j
      
      while(charTot>0){//loop goes until it reads all chars in n2
        charWS = n2.charAt(charTot-1);//reads a specfic char
        
        charTot--;//decrements to run through n2
        
        if(Character.isWhitespace(charWS)){//asks if a char is whitespace
          j++;//if is increments my counter j
        }
      }//closes loop
      
      numWords = j + 1;//defines numWords as the number of WS chars(spaces) plus one as numWords
      return numWords;//returns numWords
    }//closing  getNumOfWords
    
      
    public static int findText(String word, String n2){//opening findText method
      int instances = 0;//creates in instances
      
      int i = 0;//creates int i
      
      while(i <= (n2.length()-word.length())){//loop will run til i equals statement - word length
        if(n2.substring(i, i + word.length()).equals(word)){//gives parameters for instances incrementing
          instances++;//increments instances
        }
        i++;//increments i
      }//close loop
      
      return instances;//returns num of specific word
     }//closing findText
    
    
    public static String replaceExclamation(String n2){//opening replaceExclamation method

    String newN2 = n2.replace('!','.');//replaces all '!' with '.' in n2 in a newN2
    
    return newN2;//returns the new string
    }//closing replaceExclamation
    
    
    public static String shortSpace(String n2){//opening shortSpace method
     
      String newN2 = " ";//creating the edited version
      newN2 = n2.replace("  "," ");//replacing all double spaces with single
      
      while(newN2 != n2){//running loop while n2 and newN2 are !=
        n2 = newN2;//setting n2 = newN2
        newN2 = n2.replace("  "," ");//replacing infinitely
      }//closing while
      
     return newN2;//returning edits
    }//closing shortSpace 
}//closing public class