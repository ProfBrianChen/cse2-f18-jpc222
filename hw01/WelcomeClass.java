////////////////////
/// "WelcomeClass" Homework Asignment
/// Prints this message to the screen:
///  -----------
///  | WELCOME |
///  -----------
///  ^  ^  ^  ^  ^  ^
/// / \/ \/ \/ \/ \/ \
///<-J--P--C--2--2--2->
/// \ /\ /\ /\ /\ /\ /
///  v  v  v  v  v  v
/// Jack Curtis
/// 08/31/2018
/// CSE 02 with Professor Carr
//

public class WelcomeClass{
  public static void main(String[]args){
   
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--P--C--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}