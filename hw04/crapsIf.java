/////////////////////////
///crapsIf by Jack Curtis
///CSE002 with Professor Carr
///September 23 2018
///Function:
//////The purpose pf this program is 
///to produce a random roll of
///dice using if statements.
///The program also lets the user
///decide if they want their dice 
///to be random or not.
///Usage:
///Run the program, enter 1 or 2 
///for random or pick. then either
///get your roll or enter your 
///integers for the roll you want
//
//

import java.util.Scanner;//importing the scanner class

public class crapsIf{//opening the public class
  public static void main(String[]args){//opening the main method
   
   Scanner myScanner = new Scanner(System.in);//creating and defining the scanner
    
    System.out.println("Would you like to cast the dice randomly or state your two dice? Press 1 for random and 2 to select");//prompting for user input
    
   int choice = myScanner.nextInt();//defining user input with scanner
    
   int dieOne = 0;//creating and defining dieOne
   int dieTwo = 0;//creating and defining dieTwo
    
    if (choice == 1){//opening if state about the user input
      dieOne = (int) ((Math.random() * 5) + 1);//picking a random number [1,6]
      dieTwo = (int) ((Math.random() * 5) + 1);//picking a random number [1,6]
    }//closing if state about the user input
    
    
    else if (choice == 2){//opening if state about the user input
      System.out.println("Enter the numbers for your dice as integers: ");//prompting for further input
      dieOne = myScanner.nextInt();//defining dieOne as next input
      dieTwo = myScanner.nextInt();//defining dieTwo as next input
    }//closing if state about the user input
   
    
    if (dieOne == 1){//opening the if statement for when dieOne equals 1
      if (dieTwo == 1){//opening the if statement for when dieTwo equals 1
        System.out.println("Snake Eyes");//Prints Snake Eyes
      }//closing the if statement for when dieTwo equals 1
      else if (dieTwo == 2){//opening the if statement for when dieTwo equals 2
        System.out.println("Ace Deuce");//Prints Ace Deuce
      }//closing the if statement for when dieTwo equals 2
      else if (dieTwo == 3){//opening the if statement for when dieTwo equals 3
        System.out.println("Easy Four");//Prints Easy Four
      }//closing the if statement for when dieTwo equals 3
      else if (dieTwo == 4){//opening the if statement for when dieTwo equals 4
        System.out.println("Fever Five");//Prints Fever Five
      }//closing the if statement for when dieTwo equals 4
      else if (dieTwo == 5){//opening the if statement for when dieTwo equals 5
        System.out.println("Easy Six");//Prints Easy Six
      }//closing the if statement for when dieTwo equals 5
      else if (dieTwo == 6){//opening the if statement for when dieTwo equals 6
        System.out.println("Seven Out");//Prints Seven Out
      }//closing the if statement for when dieTwo equals 6
    }//closing the if statement for when dieOne equals 1
    
    /*The entire process above is the repeated in the
    next five else if statements with the only difference
    being that each time the value of dieOne is increased
    an increment of 1*/
    
  //The second major statement
    else if (dieOne == 2){
      if (dieTwo == 1){
        System.out.println("Ace Deuces");
      }
      else if (dieTwo == 2){
        System.out.println("Hard Four");
      }
      else if (dieTwo == 3){
        System.out.println("Fever Five");
      }
      else if (dieTwo == 4){
        System.out.println("Easy Six");
      }
      else if (dieTwo == 5){
        System.out.println("Seven Out");
      }
      else if (dieTwo == 6){
        System.out.println("Easy Eight");
      }
    }
    
  //The third major statement
    else if (dieOne == 3){
      if (dieTwo == 1){
        System.out.println("Easy Four");
      }
      else if (dieTwo == 2){
        System.out.println("Ace Deuce");
      }
      else if (dieTwo == 3){
        System.out.println("Hard Six");
      }
      else if (dieTwo == 4){
        System.out.println("Seven Out");
      }
      else if (dieTwo == 5){
        System.out.println("Easy Eight");
      }
      else if (dieTwo == 6){
        System.out.println("Nine");
      }
    }
    
  //The fourth major statement 
    else if (dieOne == 4){
      if (dieTwo == 1){
        System.out.println("Fever Five");
      }
      else if (dieTwo == 2){
        System.out.println("Easy Six");
      }
      else if (dieTwo == 3){
        System.out.println("Seven Out");
      }
      else if (dieTwo == 4){
        System.out.println("Hard Eight");
      }
      else if (dieTwo == 5){
        System.out.println("Nine");
      }
      else if (dieTwo == 6){
        System.out.println("Easy Ten");
      }
    }
    
   //The fifth major statement
    else if (dieOne == 5){
      if (dieTwo == 1){
        System.out.println("Easy Six");
      }
      else if (dieTwo == 2){
        System.out.println("Seven Out");
      }
      else if (dieTwo == 3){
        System.out.println("Easy Eight");
      }
      else if (dieTwo == 4){
        System.out.println("Nine");
      }
      else if (dieTwo == 5){
        System.out.println("Hard Ten");
      }
      else if (dieTwo == 6){
        System.out.println("Yo-leven");
      }
    }
    
   //The sixth major statement
    else if (dieOne == 6){
      if (dieTwo == 1){
        System.out.println("Seven Out");
      }
      else if (dieTwo == 2){
        System.out.println("Easy Eight");
      }
      else if (dieTwo == 3){
        System.out.println("Nine");
      }
      else if (dieTwo == 4){
        System.out.println("Easy Ten");
      }
      else if (dieTwo == 5){
        System.out.println("Yo-leven");
      }
      else if (dieTwo == 6){
        System.out.println("Boxcars");
      }
    }
    /*This statement ends the repetition as 
    now all possiblilities have been visited*/
  }//closing the main method
}//closing the public class 