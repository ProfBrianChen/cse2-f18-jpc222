///////////////////////////////
///crapsSwitch by Jack Curtis
///CSE 002 with Professor Carr
///September 23 2018
///Function:
///The purpose pf this program is 
///to produce a random roll of
///dice using switch statements.
///The program also lets the user
///decide if they want their dice 
///to be random or not.
///Usage:
///Run the program, enter 1 or 2 
///for random or pick. then either
///get your roll or enter your 
///integers for the roll you want
//
//

import java.util.Scanner;//Importing the Scanner class

public class crapsSwitch{   //opening the public class
  public static void main(String[]args){//opening the main method
    
  Scanner myScanner = new Scanner(System.in);//creating and defining the scanner
    
  System.out.println("Would you like to cast the dice randomly or state your two dice? Press 1 for random and 2 to select");//prompting the user for input
  
  int choice = myScanner.nextInt();//creating an integer for the user's input
    
  int dieOne = 0;//creating the first die
  int dieTwo = 0;//creating the second die
    
  String SE = "Snake Eyes";///
  String AD = "Ace Deuce";///
  String EF = "Easy Four";///
  String FF = "Fever Five";//Here I am
  String ES = "Easy Six";////Creating and
  String SO = "Seven Out";///Defining all
  String HF = "Hard Four";///the possible 
  String EE = "Easy Eight";//rolls and their
  String HS = "Hard Six";////names. I'm making
  String N = "Nine";/////////them variables to 
  String HE = "Hard Eight";//make the switch 
  String ET = "Easy Ten";////statements cleaner
  String HT = "Hard Ten";////
  String YL = "Yo-leven";////
  String BX = "Boxcars";/////
    
  switch(choice){//opening the switch statement for the users input
    case 1: dieOne = (int) ((Math.random() * 5) + 1);//if input is 1 dieOne equals a random number [1,6]
            dieTwo = (int) ((Math.random() * 5) + 1);//if input is 1 dieTwo equals a random number [1,6]
      break;//break in the statement
    case 2: System.out.println("Enter the numbers for your dice as integers: ");//if input is 2 user is prompted again
            dieOne = myScanner.nextInt();//saying next integer input is dieOne
            dieTwo = myScanner.nextInt();//saying next integer iput is dieTwo
      break;//break in the statement
  }//closing switch statement
   switch(dieOne){//opening the first dieOne switch statement
    case 1: switch(dieTwo){//saying if dieOne equals 1 and opening first dieTwo switch statement
              case 1: System.out.println(SE);//if dieOne=1 and dieTwo=1 SE is printed
                break;//break in the statement
              case 2: System.out.println(AD);//if dieOne=1 and dieTwo=2 AD is printed
                break;//break in the statement
              case 3: System.out.println(EF);//if dieOne=1 and dieTwo=3 EF is printed
                break;//break in the statement
              case 4: System.out.println(FF);//if dieOne=1 and dieTwo=4 FF is printed
                break;//break in the statement
              case 5: System.out.println(ES);//if dieOne=1 and dieTwo=5 ES is printed
                break;//break in the statement
              case 6: System.out.println(SO);//if dieOne=1 and dieTwo=6 SO is printed
                break;//break in the statement
                }//closing the dieTwo switch statement
              }//closing the dieOne switch statement
    switch(dieOne){                     
     case 2: switch(dieTwo){                  //This process is now repeated in these
              case 1: System.out.println(AD); //switch statements, however the value
                break;                        //of dieOne is raise by one each time
              case 2: System.out.println(HF); //Therefore altering which String is 
                break;                        //printed
              case 3: System.out.println(FF);
                break;
              case 4: System.out.println(ES);
                break;
              case 5: System.out.println(SO);
                break;
              case 6: System.out.println(EE);
                break;
                }
              }
    switch(dieOne){
      case 3: switch(dieTwo){                  //This process is now repeated in these
              case 1: System.out.println(EF); //switch statements, however the value
                break;                        //of dieOne is raise by one each time
              case 2: System.out.println(FF); //Therefore altering which String is
                break;                        //printed
              case 3: System.out.println(HS);
                break;
              case 4: System.out.println(SO);
                break;
              case 5: System.out.println(EE);
                break;
              case 6: System.out.println(N);
                break;
                }
              }
    switch(dieOne){
       case 4: switch(dieTwo){                //This process is now repeated in these
              case 1: System.out.println(FF); //switch statements, however the value
                break;                        //of dieOne is raise by one each time
              case 2: System.out.println(ES); //Therefore altering which String is
                break;                        //printed
              case 3: System.out.println(SO);
                break;
              case 4: System.out.println(HE);
                break;
              case 5: System.out.println(N);
                break;
              case 6: System.out.println(ET);
                break;
                }
              }
    switch(dieOne){
       case 5: switch(dieTwo){                //This process is now repeated in these
              case 1: System.out.println(ES); //switch statements, however the value
                break;                        //of dieOne is raise by one each time
              case 2: System.out.println(SO); //Therefore altering which String is
                break;                        //printed
              case 3: System.out.println(EE);
                break;
              case 4: System.out.println(N);
                break;
              case 5: System.out.println(HT);
                break;
              case 6: System.out.println(YL);
                break;
                }
              }
    switch(dieOne){
       case 6: switch(dieTwo){                //This process is now repeated in these
              case 1: System.out.println(SO); //switch statements, however the value
                break;                        //of dieOne is raise by one each time
              case 2: System.out.println(EE); //Therefore altering which String is
                break;                        //printed
              case 3: System.out.println(N);
                break;
              case 4: System.out.println(ET);
                break;
              case 5: System.out.println(YL);
                break;
              case 6: System.out.println(BX);
                break;
                }
              }
  }  
}