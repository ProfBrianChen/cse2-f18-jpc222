///////////////////
///Shuffling by Jack Curtis
///CSE 002
///11/13/18
///Creates and prints a 
///deck of 52 cards
///Shuffles the deck then
///reprints it
///creates unique hands of
///5 cards as the user prompts
//

import java.util.Scanner;//import scanner

public class Shuffling{ //open class
  public static void main(String[] args) { //open main meth
    
 Scanner scan = new Scanner(System.in); //Construct scanner
 
//suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
 String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
 String[] cards = new String[52]; //creates deck array
 String[] hand = new String[5]; //creates hand array
 
 int numCards = 5; //creates numCards for hands
 int again = 1; //for while repetition
 int index = 51; //used as index in shuffle meth

 for (int i=0; i<52; i++){ //prints the deck of cards
   cards[i]=rankNames[i%13]+suitNames[i/13]; 
   System.out.print(cards[i]+" "); 
 } 

 shuffle(cards);//call shuffle meth 

 System.out.println();//new line
 
 printArray(cards);//call the print meth 

 System.out.println();//new line
 
 while(again == 1){ //keeps prompting user for a new hand
   hand = getHand(cards,index,numCards);//calling getHand meth 
   printArray(hand);//calling print meth for hand
   index = index - numCards;//keeps working through the deck
   if(index<numCards){//checks if at ende of deck
     index = 51;
     shuffle(cards);//if at end makes a new unique deck
   }
   System.out.println("Enter a 1 if you want another hand drawn"); //prompt for another deck
   again = scan.nextInt(); 
}  
  }//close main meth
  
  public static void printArray(String[] list){//This method just prints array elements then a space
    for(int i = 0; i < list.length; i++){//prints each element
      System.out.print(list[i] + " ");
    }
  }//close print meth
  
  public static void shuffle(String[] deck){//open shuffle meth
    
    String temp = " "; //creates a temp value for swapping
    int j=0;//var for random index
    
    for(int i = 0; i<100; i++){ //runs 100 times for a good shuffle     
     
       int random = (int)(Math.random()* (deck.length-1)+1);//generates a random int btwn 1-52
       temp = deck[0];//sets temp to primary deck val
       j = random;//sets j to the random
       deck[0]=deck[j];//makes deck's first index random
       deck[j]=temp;//sets deck[j] to deck[0] finishing swap
    }                  
   }//close shuffle meth
  
  public static String[] getHand(String[] hand, int index, int numCards){//open get hand meth
    
   String[] lilHand = new String[numCards];//creates array lilHand
   
    for(int i =0;i<lilHand.length;i++){//fills lilHand with 5 cards at the end of deck
      lilHand[i] = hand[index];
      index--;//to go down through deck
    }
    return lilHand;//returns the 5 card hand
  }//close getHand meth   
}//close class