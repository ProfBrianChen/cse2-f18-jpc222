/////////////////////
//Arithmetic by Jack Curtis
//CSE 02 with Professor Carr
//This program takes the information
//provided in the asignment (the prices of
//the items, the number of items, and the 
//sales tax) and computes then prints the
//following:
//Pants cost $34.98 per pair.
//Sweatshirts cost $24.99 each.
//Belts cost $33.99 each.
//The sales tax on three pairs of pants is $6.29.
//The sales tax on two sweatshirts is $2.99.
//The sales tax on one belt is $2.03.
//The total cost of this purchase before sales tax is $188.91.
//The total sales tax on this purchase is $11.33.
//The total cost of this purchase after sales tax is $200.24.
//

public class Arithmetic{
  public static void main(String[]args){
    
    //Defining the input variables
    int numPants = 3;//number of pairs of pants
    double pantsPrice = 34.98;//price per pair of pants
    int numShirts = 2;//number of sweatshirts
    double shirtPrice = 24.99;//price per sweatshirt
    int numBelts = 1;//number of belts
    double beltCost = 33.99;//Price per belt
    double paSalesTax = 0.06;//PA tax rate
    
    //Total Cost of each item type before tax
    double totalPantsPrice = numPants * pantsPrice;//Cost of all the pants before tax
    double totalShirtsPrice = numShirts * shirtPrice;//Cost of all sweatshirts before tax
    double totalBeltPrice = numBelts * beltCost;//Cost of all belts before tax
    
    //Calculating tax
    double taxPants = (totalPantsPrice) * paSalesTax;//tax on pants
    double taxShirts = (totalShirtsPrice) * paSalesTax;//tax on sweatshirts
    double taxBelts = (totalBeltPrice) * paSalesTax;//tax on belts
    
    //Calculating the total cost of the purchase before and after tax
    double totalPrice = totalPantsPrice + totalBeltPrice + totalShirtsPrice;//total price before tax
    double totalTax = totalPrice * paSalesTax;//total sales tax
    double finalPrice = totalTax + totalPrice;//total price after tax
    
    //Removing extra decimals on item taxes
    int bigTaxPants = (int) (taxPants * 100);//Converts taxPants to an integer
    double adjustTaxPants = bigTaxPants / 100.0;//Converts taxPants back to a double with fewer decimal places
    int bigTaxShirts = (int) (taxShirts * 100);//Converts taxShirts to an integer
    double adjustTaxShirts = bigTaxShirts / 100.0;//Converts taxShirts back to a double with fewer decimal places
    int bigTaxBelts = (int) (taxBelts * 100);//Converts taxBelts to an integer
    double adjustTaxBelts = bigTaxBelts / 100.0;//Converts taxBelts back to a double with fewer decimal places
    
    //Removing extra decimals on the final tax and price
    int bigTax = (int) (totalTax * 100);//Converts total tax to an integer
    double adjustTax = bigTax / 100.0;//Converts tax back to a double with fewer decimal places
    int bigPrice = (int) (finalPrice * 100);//Converts finalPrice to an integer
    double adjustFinal = bigPrice / 100.0;//Converts finalPrice back to a double with fewer decimal places
    
    //Printing all of the information out
    System.out.println("Pants cost $" + pantsPrice + " per pair.");//Prints the price of pants
    System.out.println("Sweatshirts cost $" + shirtPrice + " each.");//Prints the price of sweatshirts
    System.out.println("Belts cost $" + beltCost + " each.");//Prints the price of belts
    System.out.println("The sales tax on three pairs of pants is $" + adjustTaxPants + ".");//Prints the tax on three pairs of pants
    System.out.println("The sales tax on two sweatshirts is $" + adjustTaxShirts + ".");//Prints the tax on two sweatshirts
    System.out.println("The sales tax on one belt is $" + adjustTaxBelts + ".");//Prints the tax on one belt
    System.out.println("The total cost of this purchase before sales tax is $" + totalPrice + ".");//Prints the cost of the purchase beore tax
    System.out.println("The total sales tax on this purchase is $" + adjustTax + ".");//Prints the tax on the whole purchase
    System.out.println("The total cost of this purchase after sales tax is $" + adjustFinal + ".");//Prints the final price after tax

  }
}