/////////////////////////////////
///Lab03 Check by Jack Curtis
///CSE 002 with Professor Carr
///September 14th 2018
///Function:
///
///This program should prompt a user to
///input the price of a meal
///
///Then it will ask them to input the 
///percent tip they wish to give
///
///Then it will ask the to input the
///number of people that the check will
///be split between
///
///Once it has this information it will
///Output how much money (in the form $xx.xx)
///that each person owes.


import java.util.Scanner;//Importing the scanner

public class Check{//Creating the class
  public static void main(String[]args){//Main method required for java programs
    
    Scanner myScanner = new Scanner( System.in );//declaring an instance of the scanner object
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");//Prompting the user to input check cost
    
    double checkCost = myScanner.nextDouble();//allocating memory for the check cost input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");//Prompting the user to input tip info
    
    double tipPercent = myScanner.nextDouble();//allocating memory for the tip input
    
    tipPercent /= 100; //converting tipPercent to a decimal value and defining it  
    
    System.out.print("Enter the number of people who went out to dinner: ");//prompts the user to input the number of people at dinner
     
    int numPeople = myScanner.nextInt();//allocating memory for the number of people input
    
    double totalCost;//total price of the order
    double costPerPerson;//total price of the order divided by the number of people
    int dollars, dimes, pennies;//for storing digits
    
    totalCost = checkCost * (1 + tipPercent);//defining totalCost
    costPerPerson = totalCost / numPeople;//defining costPerPerson
    dollars = (int) costPerPerson;//defining dollars
    dimes = (int) (costPerPerson * 10) % 10;//defining dimes
    pennies = (int) (costPerPerson * 100) % 10;//defining pennies
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);//Printing the result


    



    
  }
}
