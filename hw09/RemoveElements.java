///////////////////////////
///Remove Elements by Jack Curtis
///CSE 001 with Professor Carre
///This creates an int array with
///10 randomly generated elements
///then prompts the user to remove
///an element at an input index
///then produces a new array without
///the value previously at that index
///then prompts user to input an element
///to remove, then scans through and creates
///a new array with none of the iterations of
///that element
//
//


import java.util.Scanner;
import java.util.Random;//importing random number generator

public class RemoveElements{////////////////////////////////////////////////////////Code is pre written from here to:
  public static void main(String [] arg){

    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
    
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  
  
  }//////////////////////////////////////////////////////////////////////////////////////////////////////////To here
  
  public static int[] remove(int[] list, int target){//opening remove method
    
    int j = 0;//creates a counter
    int q = 0;//creates a counter for an index
    
    
    for(int i = 0; i < list.length; i++){//open for loop
      if(list[i]==target){//testing for if the target is in the array
        j++;//counting the iterations of target
      }//close if
    }//close for
    
      
    int[] listN2 = new int[list.length-j];//creating new array with adjusted length
    
    for(int k = 0; k<list.length; k++){//open for loop
      if(list[k]!=target){//testing each index for target
        listN2[q]=list[k];//if not target then new array is filled
        q++;//new array index incremented
      }//close if
    }//close for
    return listN2;//returning new adjusted array
  }//close method
  
  
  
  public static int[] delete(int[] list, int pos){//opening delet method
    
    int[] listN = new int[list.length-1];//creating new array with adjusted length
    int k = 0;//creating counter
    
    for(int j = 0; j<list.length; j++){//opening for loop
      if(j!=pos){//testing is index equals position
        listN[k]=list[j];//if not the new array is set equal to old
        k++;//increment new array index
      }//close if
    }//close for
    return listN;//return adjusted array
  }//close method
  
  public static int[] randomInput(){//opening random method
    
    Random randomGenerator = new Random();//creating a random int generator
    int random[] = new int[10];//binding the generator between 0-9
    
    for(int i = 0; i<random.length;i++){//open for loop
      int j = randomGenerator.nextInt(10);//setting a value to a random int
      random[i] = j;//filling random array with random ints
    }//close for
    return random;//return random array
  }//close method
  
}//close class