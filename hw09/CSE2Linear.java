////////////////////////////////
///CSE2Linear by Jack Curtis
///CSE02 with Professor Carre
///This prompts for an array input 
///in ascending order of size 15
///the searches for elements 
///binarily, scrambles it, then
///prompts to search again linearly
//
//
//

import java.util.Scanner;//import all

public class CSE2Linear{//open class
  public static void main(String[]args){//open main meth
    
    Scanner myScanner = new Scanner(System.in);//new scanner
    
    int[] arrayGrades = new int[15];//user's array
    
    int k = 0;//user input initialization
    
    System.out.println("Enter 15 integer values between 0 and 100 in ascending order: ");//prompt user input
    
    for(int i =0; i<15; i++){//open for to fill array
      if(myScanner.hasNextInt()){//test whether int or not
        k = myScanner.nextInt();//prompt input
          if(k>=0 && k<=100){//test if between 0 and 100
            if(i>0 && k>=arrayGrades[i-1]){//tests for ascending order       
              arrayGrades[i]= k;//if conditions met array filled
           }//close if
            else if(i==0){//allows zero
              arrayGrades[i]= k;//fills
            }//close if
            else{//prints for non ascending
              System.out.println("Grades must be entered in ascending order: ");//meaningful state
              i--;//sets for retry
            }//close else
        }//close if
          else{//prints for not 0-100
            System.out.println("Enter an Integer between 0 and 100: ");//meaningful state
            i--;//sets for retry
          }//close else
        }//close if
         else{//prints for not an int
          System.out.println("Enter an Integer value: ");//meaningful state
          myScanner.next();//prompts retry
          i--;//preps retry
        }//close else
    }//close loop
    
   for(int j = 0; j < arrayGrades.length; j++){//prints each element of new
      System.out.print(arrayGrades[j] + " ");//prints array
    }//close for
   
   System.out.println();//new line
   
   System.out.println("Enter a grade to search for: ");//prompts user to search binary
   
   int grade = myScanner.nextInt();//user input
   
   binarySearch(arrayGrades, grade);//call binarySearch
   
   System.out.println("Scrambled: ");//Meaningful State
 
   Scramble(arrayGrades);//call Scramble meth
   
   Print(arrayGrades);//print new scrambled array
   
   System.out.println();//new line
   
   System.out.println("Enter a grade to search for: ");//prompts user to search linear
   
   int grade2 = myScanner.nextInt();//user input2
   
   linearSearch(arrayGrades, grade2);//calls linear search meth
  }//close main method
 
  public static void linearSearch(int[] list, int key){//open linear search meth
    int i = 0;//making a counter
    int k = list.length;//setting length to var k
    
    for(i = 0; i<list.length; i++){//open for
      if (key==list[i]){//testing for iterations
        System.out.println(key+ " was found in the list with " + i + " iterations");//meaningful state
        k--;//decrementing length according
      }//close if
     }//close for
    
    if(i==k){//testing if no iterations found
    System.out.println(key+ " was not found in the list with " + i + " iterations");//meaningful state
    }//close if
   }//close method
   
  
  public static void Print(int[] list){//This method just prints array elements then a space
    for(int i = 0; i < list.length; i++){//prints each element
      System.out.print(list[i] + " ");
    }
  }
 
  public static void Scramble(int[] grades){//open Scramble meth
    
    int temp = 0; //creates a temp value for swapping
    int j=0;//var for random index
    
    for(int i = 0; i<100; i++){ //runs 100 times for a good scramble     
     
       int random = (int)(Math.random()* (grades.length-1)+1);//generates a random int
       temp = grades[0];//sets temp to primary grade val
       j = random;//sets j to the random
       grades[0]=grades[j];//makes grade's first index random
       grades[j]=temp;//sets grades[j] to grades[0] finishing swap
    }                  
   }//close Scramble meth
  
  public static void binarySearch(int[] grades, int key){//opening binary search method
    
    int min = 0;//setting min of array
    int max = grades.length -1;//setting max of array
    int counter = 0;//setting a counter
    
    while(max>=min){//opening a while to run for length
      int j = (int)(min+(max - min)/2);//setting j to the middle
      
      if(key==grades[j]){//testing if key is in middle
        System.out.println(key + " was found in the list with " + counter + " iterations");//meaningful state
        counter++;//increment iteration counter
        break;//leave loop
      }//close if
      
      else if(key<grades[j]){//test if key is before mid
        max = j - 1;//reset max if so
        counter++;//increment iterations
      }//close if
      
      else if(key>grades[j]){//testing if key after mid
        min = j + 1;//reset min if so
        counter++;//increment iterations
      }//close if
      
      else{//testing if key not present
       System.out.println(key+" was not found in the list with " + counter + " iterations");//meaningful state
       break;//leave loop
      }//close else
    }//close loop
    
    if(max<min){//testing if the key was never in from start
      System.out.println(key+" was not found in the list with " + counter + " iterations");//meaningful state
    }//close if   
   }//close method   
}//close class