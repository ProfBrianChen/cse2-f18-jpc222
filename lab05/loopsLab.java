//////////////////////////////////
///loops Lab by Jack Curtis
///CSE 022 with Professor Carr
///October 7th 2018
///
///
///
///
///
///
///
///
///
///
//

import java.util.Scanner;//import scanner class

public class loopsLab{//opens public class
  public static void main(String[]args){//opens main method
    
    Scanner myScanner = new Scanner(System.in);//initializing and constructing scanner
    
    System.out.println("Please input your course number, department name, meetings " +
                       "per week, hour of start time, your instructor's name, and the number of students in your class: ");//prompting for input
  
    int courNum = 0;
    while(true){
      boolean correctCourInt = myScanner.hasNextInt();
      if(correctCourInt){
       courNum = myScanner.nextInt();
       break;
         }
      else{
        myScanner.next();
        System.out.println("You've enterend an invalid course number, please try again: ");
         } 
    }
    
   String depName = myScanner.next();//defining department name
   
   int meetWeek = 0;
   while(true){
    boolean correctMeetInt = myScanner.hasNextInt();
    if(correctMeetInt){
     meetWeek = myScanner.nextInt();
     break;
       }
    else{
      myScanner.next();
      System.out.println("You've enterend an invalid number of meetings, please try again: ");
       } 
    }
    
    int start = 0;
    while(true){
    boolean correctStartInt = myScanner.hasNextInt();
    if(correctStartInt){
     start = myScanner.nextInt();
     break;
       }
    else{
      myScanner.next();
      System.out.println("You've enterend an invalid start time, please try again: ");
       } 
    }
    
    String insName = myScanner.next();//defining instructor name
    
    int numStud = 0;
     while(true){
    boolean correctStudInt = myScanner.hasNextInt();
    if(correctStudInt){
     numStud = myScanner.nextInt();
     break;
       }
    else{
      myScanner.next();
      System.out.println("You've enterend an invalid number of students, please try again: ");
       } 
    }
    
    
    System.out.println("Course number: " +courNum +" "+ "Department: " +depName +" "+ "Weekly meetings: " + meetWeek +" "+
                       "Start hour: " + start +" "+ "Instructor name: " +insName +" "+"Number of students: " +numStud);//outputing valid input  
  }
}