//////////////////////////
///cardGenerator by Jack Curtis
///CSE002 with Professor Carr
///September 21st 2018
///Function:
///Genrates a random number
///between 1 and 52 then uses 
///modulus to give a suit and 
///a card type 
///Usage:
///Run and a random card will generate


public class cardGenerator{               //opening the public class
  public static void main(String[]args){   //opening the main method
    
    int suits = (int) (((Math.random() * 51) + 0) % 4);//generates a random number between 0 and 3
    int cardNumber = (int) (((Math.random() * 51) + 0) % 13);//generates a random number between 0 and 12
    
    String suit = "";//creates the suit variable
    String type = "";//creates the type variable
    
    switch(cardNumber){//opens switch statement for diamonds
      case 0: type = "Ace";
        break;
      case 1: type = "2";
        break;
      case 2: type = "3";
        break;
      case 3: type = "4";
        break;
      case 4: type = "5";
        break;
      case 5: type = "6";
        break;
      case 6: type = "7";
        break;
      case 7: type = "8";
        break;
      case 8: type = "9";
        break;
      case 9: type = "10";
        break;
      case 10: type = "Jack";
        break;
      case 11: type = "Queen";
        break;
      case 12: type = "King";
        break;
    }//closes the cardNumber switch statement
    switch(suits){//opens the switch statement for cards
      case 0: suit = "Diamonds";
        break;
      case 1: suit = "Clubs";
        break;
      case 2: suit = "Hearts";
        break;
      case 3: suit = "Spades";
        break;
    }//closes the suits switch statement
   System.out.println("You picked the " + type + " of " + suit);//Outputs a random card
  }//closes main method
}//closes public class