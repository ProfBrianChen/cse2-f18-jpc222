/////////////////
// CSE 02 September 7th 2018
// Lab02 Cyclometer
// Records: time elapsed in seconds &
// Rotations of the front wheel during that time
// Prints: Number of minutes for each trip
// Number of counts for each trip
// The distance of each trip in miles
// The distance of two trips combined
// Jack Curtis Professor Carr
//
public class Cyclometer {
  //main method required for every java program
  public static void main(String[] args) {
   //Our input data
   int secsTrip1=480;// allocates space for the integer secsTrip1
   int secsTrip2=3220;// allocates space for the integer secsTrip2
   int countsTrip1=1561;// allocates space for the integer countsTrip1
   int countsTrip2=9037;// allocates space for the integer countsTrip2
   
    //Our intermediate variables and output data
   double WheelDiameter=27.0;// allocates space for the double WheelDiameter
   double PI=3.141589;// allocates space for the double PI
   double feetPerMile=5280;// allocates space for the double feetPerMile
   double inchesPerFoot=12;// allocates space for the double inchesPerFoot
   double secondsPerMinute=60;// allocates space for the double secondsPerMinute
   double distanceTrip1, distanceTrip2, totalDistance;/* allocates space for doubles:
                                                       distanceTrip1, distanceTrip2, and totalDistance*/
    
    //Prints stored numbers and variables that store number of seconds and the counts
   System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute) + 
        " minutes and had " + countsTrip1 + " counts.");// Prints the statement: "Trip 1 took 8.0 minutes and had 1561 counts."
   System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute) +
        " minutes and had "+ countsTrip2 +" counts.");// Prints the statement: "Trip 2 took 53.666666666666664 minutes and had 9037 counts."
    
    //Defines what variables distanceTrip1, distanceTrip2, and totalDistance are
    distanceTrip1=countsTrip1*WheelDiameter*PI/inchesPerFoot/feetPerMile;// Defines distanceTrip1
    distanceTrip2=countsTrip2*WheelDiameter*PI/inchesPerFoot/feetPerMile;// Defines distanceTrip2
    totalDistance=distanceTrip1+distanceTrip2;// Defines totalDistance
   
    //Prints the distance of each trip in miles
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");// Prints the statement: "Trip 1 was 2.0897814328125004 miles."
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");// Prints the statement: "Trip 2 was 12.0982413890625 miles."
    System.out.println("The total distance was " + totalDistance + " miles.");// Prints the statement: "The total distance was 14.188022821875 miles."
    
  } //end of main method
} //end of class