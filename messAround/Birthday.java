//////////////////
//By Jack Curtis
//September 19th 2018
//User inputs bith month as integer
//Program outputs birth month
//Uses Switch statements
//

import java.util.Scanner;

public class Birthday{
  public static void main(String[]args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter your birth month as a number: ");
    
    int month = myScanner.nextInt();
    
    String monthString;
      
    switch(month){
      case 1: monthString = "January";
        break;
      case 2: monthString = "February";
        break;
      case 3: monthString = "March";
        break;
      case 4: monthString = "April";
        break;
      case 5: monthString = "May";
        break;
      case 6: monthString = "Juner";
        break;
      case 7: monthString = "July";
        break;
      case 8: monthString = "August";
        break;
      case 9: monthString = "September";
        break;
      case 10: monthString = "October";
        break;
      case 11: monthString = "November";
        break;
      case 12: monthString = "December";
        break;
      default: monthString = "Invalid month";
        break;
            }
    System.out.println("You were born in " + monthString);
  }
}