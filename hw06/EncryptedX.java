////////////////////////////
///EncryptedX by Jack Curtis
///Due 10/23/2018
///CSE 002 with Professor Carre
///Function:
///Asks user to input an 
///integer between 1 and 100
///Then generates a square 
///with dimensions of the
///user's input and an X
///built inside of it
//

import java.util.Scanner;//importing the scanner class

public class EncryptedX{//opening the public class
  public static void main(String[]args){//opening the main method
    
    Scanner myScanner = new Scanner(System.in);//Creating the Scanner
    
    System.out.println("Input an integer between one and one-hundred: ");//prompting for user input
    
   int stars = 0;//creates an integer stars
   while(true){//infinite loop
    boolean correctStars = myScanner.hasNextInt();//checks if input is an int
    if(correctStars){
      stars = myScanner.nextInt();//if int input equals stars
      if(-1<stars && stars<101){//makes sure its between 1-100
        break;
      }//closing nested if
      else{
        System.out.println("Please input an integer between 1-100");//prints error if not between 1 and 100
        myScanner.next();
        correctStars = myScanner.hasNextInt();
      }//closing nested else
    }//closing if 
    else{
      System.out.println("Please input an integer between 1-100");//prints error if not an int
      myScanner.next();
      correctStars = myScanner.hasNextInt();
    }//closing else  
   }//closing while
 
   
   for(int j=0;j<stars;j++){//this for loop makes a new row for the input number
     for(int i=0;i<stars;i++){//this loop makes the correct number of columns
       if(i == j || i==stars-j-1)//puts spaces in the coresponding points in the square
       {System.out.print(" ");}// to create an X
       else
       {System.out.print("*");}//Prints the stars to make the box
     }
     System.out.println();//The new line
   }  
  }//closing main method
}//closing public class
