///////////////////////////////
/// Display Pyramids by Jack Curtis
///CSE 002 with Professor Carr
///10/12/2018
///Prints some
///number
///pyramids
//

import java.util.Scanner;

public class displayPyramids{
  public static void main(String[]args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Input the number of rows for your pyramid(1-10): ");
    
    int numRows = 0;
    
    while(true){
    boolean correctRows = myScanner.hasNextInt();
    if(correctRows){
      numRows = myScanner.nextInt();
      if(0<numRows && numRows<11){
        break;
      }//closing nested if
      else{
        System.out.println("Please input an integer between 1-10");
        myScanner.next();
        correctRows = myScanner.hasNextInt();
      }//closing nested else
    }//closing if 
    else{
      System.out.println("Please input an integer between 1-10");
      myScanner.next();
      correctRows = myScanner.hasNextInt();
    }//closing else  
   }//closing while

    //Pattern A
    for(int A=1;A<=numRows;A++){
      for(int z =1;z<=A;z++){
        System.out.print(z + " ");
      }
     System.out.println();
    }
   

   System.out.println();


    //Pattern B
    int i= numRows;
    for(int B=1;B<=numRows;B++){
      for(int y=1;y<=i;y++){
        System.out.print(y + " ");
      }
      System.out.println();
      i--;
    }

    System.out.println();

    //Pattern C
    int l = 1;
    for(int C=1;C<=numRows;C++){
      for(int x =l;x>0;x--){
        System.out.print(x + " ");
          }
       System.out.println();
       l++;
     }

    System.out.println();


    //Pattern D
    int j = numRows;
    for(int D=1;D<=numRows;D++){
      for(int w =j;w>0;w--){
         System.out.print(w + " ");
        }
        System.out.println();
        j--;
      }
  }
}